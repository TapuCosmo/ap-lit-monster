"use strict";

import {Howl} from "howler";

/*
  Sound Effects
*/

const sounds = {
  click: new Howl({
    src: [
      "soundEffects/click.ogg",
      "soundEffects/click.wav"
    ]
  }),
  poweringUp: new Howl({
    src: [
      "soundEffects/poweringUp.ogg",
      "soundEffects/poweringUp.wav"
    ],
    loop: true
  }),
  quake: new Howl({
    src: [
      "soundEffects/quake.ogg",
      "soundEffects/quake.wav"
    ],
    loop: true
  }),
  soundTest: new Howl({
    src: [
      "soundEffects/soundTest.ogg",
      "soundEffects/soundTest.wav"
    ]
  }),
  whoosh: new Howl({
    src: [
      "soundEffects/whoosh.ogg",
      "soundEffects/whoosh.wav"
    ]
  })
};

export default sounds;
