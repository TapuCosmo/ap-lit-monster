"use strict";

import sounds from "@/sounds.js";
import store from "./store";

/*
  Node Format:
  {
    name: String,
    messages: [{
      text: String,
      speaker: ?String,
      fontSize: ?String (default: "24px"), // Note: non-default sizes will break word wrapping
      preTypeDelay: ?Number (default: 300),
      italicText: ?Boolean (default: false),
      boldText: ?Boolean (default: false),
      continuation ?Boolean (default: false)
    }],
    nextNode: ?String, // Set this for "click anywhere to continue"
    choices: ?[{
      text: String,
      linkedNode: String,
      width: ?String,
      onclick: ?function
    }],
    textInputs: ?[{
      label: String,
      submitButtonText: String,
      placeholder: ?String,
      onsubmit: function // Input string as only argument
    }],
    save: ?Boolean (default: true),
    clickEveryMessage: ?Boolean (default: true),
    noInteractionExit: ?Boolean (default: false),
    endOfNodeDelay: ?Number (default: 0),
    onload: ?function,
    afterExit: ?function,
    messageGroupOuterContainerClass: ?String, // Applies to click to continue message
    messageGroupContainerClass: ?String, // Does not apply to click to continue message
    exitAnimationClass: ?String, // Does not apply to the green border around the messages
    displayExitAnimationClass: ?String, // Applies to the entire NodeDisplay,
    removeMessageGroupAnimationOnExitDelay: ?Number (default: 0),
    backgroundSound: ?Howl,
    exitSound: ?Howl
  }
*/

const nodes = [
  // Start Screens
  {
    name: "splash-0",
    messages: [
      {
        speaker: "FILE 65535: THE MONSTER",
        fontSize: "64px"
      },
      {
        text: " ",
        fontSize: "64px"
      },
      {
        text: "An AP English Literature Project",
        fontSize: "48px"
      },
      {
        text: " ",
        fontSize: "64px"
      },
      {
        speaker: "By",
        text: "Puneet Kaler and Vietbao Tran",
        fontSize: "48px"
      },
      {
        text: " ",
        fontSize: "64px"
      },
      {
        text: "Hosted at https://tapucosmo.gitlab.io/ap-lit-monster/",
        fontSize: "36px"
      },
      {
        text: " ",
        fontSize: "64px"
      },
      {
        text: "root@universe:/# systemctl start universe",
        fontSize: "36px"
      }
    ],
    choices: [
      {
        text: "Sound Test",
        width: "12ch",
        onclick() {
          sounds.soundTest.play();
        }
      },
      {
        text: "Open File!",
        linkedNode: "splash-1",
        width: "12ch"
      },
      {
        text: "Source Code",
        width: "12ch",
        onclick() {
          window.open("https://gitlab.com/TapuCosmo/ap-lit-monster", "_blank");
        }
      }
    ],
    save: false,
    clickEveryMessage: false
  },
  {
    name: "splash-1",
    messages: [
      {
        text: "Opening File 65535..."
      }
    ],
    choices: [
      {
        text: "Wipe Data",
        linkedNode: "reset-0"
      }
    ],
    save: false,
    nextNode: "chooseName-0",
    noInteractionExit: true,
    clickEveryMessage: false,
    endOfNodeDelay: 2000,
    exitAnimationClass: "fadeOut-1s",
    onload() {
      if (window.localStorage.currentNode) {
        this.nextNode = window.localStorage.currentNode;
      }
    },
    afterExit() {
      this.nextNode = "chooseName-0";
    }
  },
  // Data Wipe
  {
    name: "reset-0",
    messages: [
      {
        speaker: "Are you sure you want to delete your saved data?"
      }
    ],
    choices: [
      {
        text: "Yes",
        linkedNode: "reset-1",
        width: "5ch"
      },
      {
        text: "No",
        linkedNode: "splash-0",
        width: "5ch"
      }
    ],
    save: false,
    clickEveryMessage: false
  },
  {
    name: "reset-1",
    messages: [
      {
        speaker: "Are you really sure you want to wipe your saved data?"
      }
    ],
    choices: [
      {
        text: "Yes",
        linkedNode: "reset-2",
        width: "5ch",
        onclick() {
          window.localStorage.currentNode = "";
          window.localStorage.playerName = "";
          window.localStorage.blobName = "";
        }
      },
      {
        text: "No",
        linkedNode: "splash-0",
        width: "5ch"
      }
    ],
    save: false,
    clickEveryMessage: false
  },
  {
    name: "reset-2",
    messages: [
      {
        speaker: "Your saved data has been wiped."
      },
      {
        text: "Returning to start screen..."
      }
    ],
    save: false,
    clickEveryMessage: false,
    noInteractionExit: true,
    endOfNodeDelay: 1500,
    exitAnimationClass: "fadeOut-1s",
    nextNode: "splash-0"
  },
  {
    name: "chooseName-0",
    messages: [
      {
        text: "What is your name?"
      }
    ],
    textInputs: [
      {
        label: "Your Name:",
        submitButtonText: "Proceed",
        placeholder: "Enter your name...",
        onsubmit(name) {
          store.commit("setPlayerName", name);
          goToNode("chooseName-1");
        }
      }
    ],
    clickEveryMessage: false
  },
  {
    name: "chooseName-1",
    messages: [
      {
        text: "So you are... {{PLAYER_NAME}}?"
      }
    ],
    choices: [
      {
        text: "Yes",
        linkedNode: "prologue-0",
        width: "5ch"
      },
      {
        text: "No",
        linkedNode: "chooseName-0",
        width: "5ch"
      }
    ],
    save: false,
    clickEveryMessage: false,
    exitAnimationClass: "fadeOut-1s",
  },
  // Prologue
  {
    name: "prologue-0",
    messages: [
      {
        text: "Approximately 13.8 billion years ago...",
        italicText: true
      },
      {
        speaker: "???",
        text: "...",
        boldText: true
      },
      {
        speaker: "???",
        text: "It’s... almost... done...",
        boldText: true
      },
      {
        speaker: "???",
        text: "This universe... is ready... to form...",
        boldText: true
      }
    ],
    nextNode: "prologue-1"
  },
  {
    name: "prologue-1",
    messages: [
      {
        speaker: "???",
        text: "NOW!!!",
        fontSize: "128px",
        boldText: true
      }
    ],
    messageGroupOuterContainerClass: "quake-1s",
    messageGroupContainerClass: "blurredBeat-1_5s",
    fullExitAnimationClass: "full-whiteFadeOut-5s",
    exitAnimationClass: "fadeOut-2_5s",
    removeMessageGroupAnimationOnExitDelay: 2500,
    backgroundSound: sounds.quake,
    exitSound: sounds.whoosh,
    nextNode: "prologue-2"
  },
  {
    name: "prologue-2",
    messages: [
      {
        text: "1 Planck time later...",
        italicText: true
      },
      {
        text: "The universe was created.",
        italicText: true
      },
      {
        speaker: "???",
        text: "...",
        boldText: true
      },
      {
        speaker: "???",
        text: "I'm... so... exhausted...",
        boldText: true
      },
      {
        speaker: "???",
        text: "Need... a... long... rest...",
        boldText: true
      },
      {
        speaker: "???",
        text: "...",
        boldText: true
      }
    ],
    exitAnimationClass: "fadeOut-1s",
    nextNode: "prologue-3"
  },
  {
    name: "prologue-3",
    messages: [
      {
        text: "And so, the universe was formed.",
        italicText: true
      },
      {
        text: "As billions of years passed, stars began to form, and with them, planets.",
        italicText: true
      },
      {
        text: "Many of these planets would harbor the beginnings of life.",
        italicText: true
      },
      {
        text: "Life on one of these planets in particular, known as Earth, would gain the ability to evolve over many eons rather than withering away into dust.",
        italicText: true
      },
      {
        text: "Eventually, one primordial creature on Earth would evolve to contain a stable quantum system inside itself.",
        italicText: true
      },
      {
        text: "This creature was the second conscious entity to exist in this universe.",
        italicText: true
      },
      {
        text: "And so, life continued to evolve into more and more complex systems over the next billion years.",
        italicText: true
      },
      {
        text: "Then, a race of intelligent animals known as humans rose up and become the first pioneers of the universe...",
        italicText: true
      }
    ],
    exitAnimationClass: "fadeOut-2_5s",
    nextNode: "intro-0"
  },
  // Intro
  {
    name: "intro-0",
    messages: [
      {
        text: "January 20th, 4242 CE...",
        italicText: true
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "(Zzz... Zzz... Zzz...)"
      },
      {
        speaker: "Doug",
        text: "{{PLAYER_NAME}}, wake up! It's morning!"
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "(Aargh... Wait... Who was that? Who's in my house?!)"
      },
      {
        speaker: "Doug",
        text: "Did you seriously fall asleep in this lab? You shouldn't exert yourself so much. You'll work yourself to death!"
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "I'm sorry for worrying you... I'm trying to earn enough money to put food on my table. Housing is so expensive nowadays, and I have so much college debt!"
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Also, I really want to finish this project. If we successfully create a 1-kilogram blob of quark-gluon plasma, we can perform so many experiments on it. We can even win a Nobel Prize!"
      }
    ],
    nextNode: "intro-1"
  },
  {
    name: "intro-1",
    messages: [
      {
        text: "Karen walks into the lab.",
        italicText: true
      },
      {
        speaker: "Karen",
        text: "You know you don't get overtime for sleeping on the job, right?"
      },
      {
        speaker: "Karen",
        text: "And it will be MY Nobel Prize, not yours. I'm the one leading this project, remember?"
      },
      {
        speaker: "Loudspeaker",
        text: "Ding!",
        italicText: true
      },
      {
        speaker: "Loudspeaker",
        text: "A Presidential Emergency Alert is being broadcasted!"
      }
    ],
    nextNode: "intro-2"
  },
  {
    name: "intro-2",
    messages: [
      {
        speaker: "PRESIDENTIAL EMERGENCY ALERT"
      },
      {
        text: "If you or a loved one had bought Russian vodka within the last 90 days, you may be entitled to financial compensation."
      },
      {
        text: "Russia has resorted to poisoning the citizens of our great country in an attempt to incite panic."
      },
      {
        text: "If you consume Russian vodka and experience vomiting, headaches, or abnormal heart rhythms, dial 911 immediately."
      },
      {
        text: "Stores are required to remove all Russian vodka."
      },
      {
        text: "Congress will create a fund to compensate all victims of this horrendous attack on democracy."
      },
      {
        text: "Last, but certainly not least, thank you to all of our brave soldiers who have enlisted."
      },
      {
        text: "Our nation WILL be the victors of this Forty-Second World War!"
      }
    ],
    nextNode: "intro-3"
  },
  {
    name: "intro-3",
    messages: [
      {
        speaker: "Doug",
        text: "So... looks like we have to cancel the party we planned for after we run the experiment today."
      },
      {
        speaker: "Karen",
        text: "It's time to work now. Worry about your party on your own time."
      },
      {
        speaker: "Karen's Phone",
        text: "Do Dee Doo",
        italicText: true
      },
      {
        speaker: "Karen",
        text: "Someone's calling me. Get to work and prepare the experiment."
      },
      {
        text: "Karen leaves the room to take the call.",
        italicText: true
      },
      {
        speaker: "Karen",
        text: "...yes...development...nicely...President..."
      }
    ],
    nextNode: "intro-4"
  },
  {
    name: "intro-4",
    messages: [
      {
        speaker: "Doug",
        text: "Well, let's finish the last few preparations for the experiment."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Sure. I'll ready the magnetic containment field. You can prepare the matter and antimatter beams."
      }
    ],
    nextNode: "intro-5",
    exitAnimationClass: "fadeOut-2_5s"
  },
  {
    name: "intro-5",
    messages: [
      {
        speaker: "Computer",
        text: "[00:00:00] Initialization sequence started by user {{PLAYER_NAME}}."
      },
      {
        speaker: "Computer",
        text: "[00:00:02] Beginning to pull vacuum in collision chamber..."
      },
      {
        speaker: "Computer",
        text: "[04:32:25] Collision chamber pressure has dropped to 0.000000001 atm."
      },
      {
        speaker: "Computer",
        text: "[04:32:30] Starting liquid helium dump into magnetic coils..."
      },
      {
        speaker: "Computer",
        text: "[06:43:11] Magnetic coils have cooled to 2 K and are now superconducting."
      },
      {
        speaker: "Computer",
        text: "[06:43:17] Injecting current into magnetic coils..."
      },
      {
        speaker: "Computer",
        text: "[06:51:32] Magnetic coils have stabilized at 537 T."
      },
      {
        speaker: "Computer",
        text: "[06:53:43] User Doug commanded 10 kg of U-232 to be loaded into Beam Container A from Trap 23."
      },
      {
        speaker: "Computer",
        text: "[06:54:32] User Doug commanded 10 kg of anti-U-232 to be loaded into Beam Container B from Trap 58."
      },
      {
        speaker: "Computer",
        text: "[07:12:58] Container A has been filled."
      },
      {
        speaker: "Computer",
        text: "[07:14:38] Container B has been filled."
      },
      {
        speaker: "Computer",
        text: "[07:14:24] Uncovering glass roof..."
      },
      {
        speaker: "Computer",
        text: "[07:17:11] Glass roof fully uncovered."
      },
      {
        speaker: "Computer",
        text: "[07:17:24] Preparations done. Waiting for command to start reaction sequence..."
      }
    ],
    nextNode: "intro-6"
  },
  {
    name: "intro-6",
    messages: [
      {
        speaker: "Karen",
        text: "FINALLY, it's ready! Just need to configure a few more things..."
      },
      {
        speaker: "Computer",
        text: "[07:24:57] User Karen set output beam target to asteroid 2938471 (altitude 73°, azimuth 147°)."
      },
      {
        speaker: "Computer",
        text: "[07:25:03] Running orbital simulations..."
      },
      {
        speaker: "Computer",
        text: "[07:26:22] Orbital simulations complete. 99.2% confidence of no harm to United States assets."
      },
      {
        speaker: "Doug",
        text: "Umm... why are we aiming the energy beam towards an asteroid?"
      },
      {
        speaker: "Karen",
        text: "Oh, just getting a secondary experiment done to determine the viability of using energy beams to deflect asteroids."
      },
      {
        speaker: "Karen",
        text: "Everything is now ready to go. {{PLAYER_NAME}}, hit the button. I will monitor the data we receive from the experiments."
      },
      {
        text: "{{PLAYER_NAME}} pushes the button.",
        italicText: true
      }
    ],
    nextNode: "intro-7"
  },
  {
    name: "intro-7",
    messages: [
      {
        speaker: "Computer",
        text: "[07:31:18] Firing matter beams..."
      },
      {
        speaker: "Computer",
        text: "1.274 kg of quark-gluon plasma confined in magnetic trap."
      },
      {
        speaker: "Computer",
        text: "Performing experiments 1-57..."
      },
      {
        speaker: "Computer",
        text: "Experiment 57 completed."
      },
      {
        speaker: "???",
        text: "...EN..ER..GY..."
      },
      {
        speaker: "Computer",
        text: "Upper magnetic containment field breached. Directing energy outburst towards target..."
      },
      {
        speaker: "Computer",
        text: "Glass roof was obliterated by energy beam."
      },
      {
        text: "The ground's rumbling intensifies greatly.",
        italicText: true
      },
      {
        speaker: "Karen, Doug, and {{PLAYER_NAME}}",
        text: "AAAAAAAAAHHHHHHHH!!!",
        boldText: true
      },
      {
        text: "BOOM!",
        fontSize: "64px",
        boldText: true
      }
    ],
    messageGroupOuterContainerClass: "heavyQuake-1s",
    messageGroupContainerClass: "blurredBeatFlash-1_5s",
    fullExitAnimationClass: "full-whiteFadeOut-5s",
    exitAnimationClass: "fadeOut-2_5s",
    removeMessageGroupAnimationOnExitDelay: 2500,
    backgroundSound: sounds.quake,
    exitSound: sounds.whoosh,
    nextNode: "intro-8"
  },
  {
    name: "intro-8",
    messages: [
      {
        speaker: "???",
        text: "Ahh... refreshing energy..."
      },
      {
        speaker: "???",
        text: "This new form... feels nice..."
      },
      {
        speaker: "???",
        text: "So this is the beautiful universe I created..."
      },
      {
        speaker: "???",
        text: "I finally see it, after 13.8 billion years..."
      },
      {
        speaker: "???",
        text: "I sense the presence of the one who summoned me..."
      },
      {
        text: "The mysterious blob and {{PLAYER_NAME}} teleport to a desolate forest.",
        italicText: true
      }
    ],
    messageGroupContainerClass: "blurredBeatFlash-1_5s",
    fullExitAnimationClass: "full-whiteFadeOut-5s",
    exitAnimationClass: "fadeOut-2_5s",
    exitSound: sounds.whoosh,
    nextNode: "intro-9"
  },
  {
    name: "intro-9",
    messages: [
      {
        speaker: "{{PLAYER_NAME}}",
        text: "(Where am I?)"
      },
      {
        speaker: "???",
        text: "Hello!"
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Eeeeeek!"
      },
      {
        speaker: "???",
        text: "Calm down. Don't be afraid. You summoned me back to existence."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Who are you?"
      },
      {
        speaker: "???",
        text: "I am... wait I don't have a name. Perhaps you can name me?"
      }
    ],
    nextNode: "chooseBlobName-0"
  },
  {
    name: "chooseBlobName-0",
    messages: [
      {
        speaker: "???",
        text: "{{PLAYER_NAME}}, what do you wish to name me?"
      }
    ],
    textInputs: [
      {
        label: "Name:",
        submitButtonText: "Proceed",
        placeholder: "Enter a name...",
        onsubmit(name) {
          store.commit("setBlobName", name);
          goToNode("chooseBlobName-1");
        }
      }
    ],
    clickEveryMessage: false
  },
  {
    name: "chooseBlobName-1",
    messages: [
      {
        speaker: "???",
        text: "So you wish to name me... {{BLOB_NAME}}?"
      }
    ],
    choices: [
      {
        text: "Yes",
        linkedNode: "asteroid-0",
        width: "5ch"
      },
      {
        text: "No",
        linkedNode: "chooseBlobName-0",
        width: "5ch"
      }
    ],
    save: false,
    clickEveryMessage: false,
    exitAnimationClass: "fadeOut-1s",
  },
  {
    name: "asteroid-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "So I am {{BLOB_NAME}} now..."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Anyway, I was awoken when your experiment happened to create an area with a stable quantum state. This reconstructed my consciousness."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Basically, I am like a Boltzmann brain, but more stable. Have you ever wondered how free will works?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Free will can't come from classical mechanics, since the past and future of a classical system is completely deterministic."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Therefore, free will, if it exists, must arise from the nondeterministic nature of quantum mechanics."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "And so, you and I are both conscious thanks to our abilities to maintain a stable quantum system within ourselves."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "The other possibility is that free will is an illusion, and I do not want to think about that."
      }
    ],
    nextNode: "asteroid-1"
  },
  {
    name: "asteroid-1",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "When I was reawoken, a beam of intense energy was launched toward an asteroid."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "That asteroid was deflected and is now on a collision course with Earth."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "More precisely, it will strike Moscow, Russia and obliterate everything within a distance of 1000 kilometers in 5 hours."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Oh... did Karen do this on purpose?!"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "The human you refer to as Karen is involved with the top members of your government and military. She was ordered to do this."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Wow, I had no idea all this time."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "The project you were working on was the ultimate weapon. It was designed to defend the Earth against extraterrestrial threats, but was adapted for war instead."
      }
    ],
    nextNode: "asteroid-2"
  },
  {
    name: "asteroid-2",
    messages: [
      {
        speaker: "{{PLAYER_NAME}}",
        text: "So what do we do now?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "I have near-infinite power. Ask me, and I can do anything for you."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Really?!"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Yes. You have 2 choices."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "You can either end this war with peace, or you can end it with violence."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Choose peace, and I will destroy the asteroid and both sides' weapons."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Choose violence, and your opponents will be destroyed."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Make your choice now."
      }
    ],
    choices: [
      {
        text: "Peace",
        linkedNode: "asteroid-peace-0",
        width: "10ch"
      },
      {
        text: "Violence",
        linkedNode: "asteroid-violence-0",
        width: "10ch"
      }
    ]
  },
  {
    name: "asteroid-peace-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "You have chosen to end the war through peace."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "How do you wish for peace to be achieved?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Do you want weapons to turn into water when used?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Or do you wish for them to shoot tree seeds into the ground?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Maybe you just want weapons to emit bad jokes?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Your last option is to make weapons turn surrounding pollutants into 3D printing material."
      }
    ],
    choices: [
      {
        text: "Water",
        linkedNode: "asteroid-peace-water-0",
        width: "18ch"
      },
      {
        text: "Tree Seeds",
        linkedNode: "asteroid-peace-seeds-0",
        width: "18ch"
      },
      {
        text: "Bad Jokes",
        linkedNode: "asteroid-peace-jokes-0",
        width: "18ch"
      },
      {
        text: "Printing Material",
        linkedNode: "asteroid-peace-print-0",
        width: "18ch"
      }
    ],
    fullExitAnimationClass: "full-whiteFadeOut-5s",
    exitAnimationClass: "fadeOut-2_5s"
  },
  {
    name: "asteroid-violence-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "You have chosen to end the war through violence."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "The asteroid will be left on its path to impact Moscow."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "But the asteroid alone will not be sufficient to end the war."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "I can end the war by detonating all nuclear missiles in Russia or by turning Russia into ice."
      }
    ],
    choices: [
      {
        text: "Detonate Missiles",
        linkedNode: "asteroid-violence-missiles-0",
        width: "18ch"
      },
      {
        text: "Convert to Ice",
        linkedNode: "asteroid-violence-ice-0",
        width: "18ch"
      }
    ]
  },
  {
    name: "asteroid-violence-missiles-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "Let your will be done."
      },
      {
        text: "The asteroid teleports to right above Moscow and immediately impacts the city.",
        italicText: true
      },
      {
        text: "All of the nuclear missiles stored in Russia simultaneously detonate.",
        italicText: true
      },
      {
        text: "A mild earthquake can be felt across the entire world.",
        italicText: true
      },
      {
        text: "When all is done, all that is left of Russia is rubble.",
        italicText: true
      }
    ],
    messageGroupOuterContainerClass: "quake-1s",
    fullExitAnimationClass: "full-whiteFadeOut-5s",
    exitAnimationClass: "fadeOut-2_5s",
    removeMessageGroupAnimationOnExitDelay: 2500,
    backgroundSound: sounds.quake,
    exitSound: sounds.whoosh,
    nextNode: "choosePath-ideals/power-0"
  },
  {
    name: "asteroid-violence-ice-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "Let your will be done."
      },
      {
        text: "The asteroid teleports to right above Moscow and immediately impacts the city.",
        italicText: true
      },
      {
        text: "Once the thermal blast from the asteroid is over, thermal energy is extracted from all objects in Russia, including moisture in the air.",
        italicText: true
      },
      {
        text: "A bright flash of infrared light bursts from Russia and is detected by satellites.",
        italicText: true
      },
      {
        text: "The frozen moisture falls on top of Russia, where it forms a layer of thick ice.",
        italicText: true
      },
      {
        text: "A strong wind blows over Russia as air rushes in to fill the space where water vapor used to be.",
        italicText: true
      }
    ],
    messageGroupOuterContainerClass: "quake-1s",
    fullExitAnimationClass: "full-whiteFadeOut-5s",
    exitAnimationClass: "fadeOut-2_5s",
    removeMessageGroupAnimationOnExitDelay: 2500,
    backgroundSound: sounds.quake,
    exitSound: sounds.whoosh,
    nextNode: "choosePath-ideals/power-0"
  },
  {
    name: "asteroid-peace-water-0",
    messages: [
      {
        text: "United States Nuclear Facility #251",
        italicText: true
      },
      {
        speaker: "Alarm",
        text: "Flood detection sensors triggered in nuclear missile storage area."
      },
      {
        speaker: "Commander",
        text: "What on Earth is going on? We've been in a drought for the last 37 years, where is this flood coming from?"
      },
      {
        text: "The Commander goes to investigate the storage area.",
        italicText: true
      },
      {
        speaker: "Commander",
        text: "WHERE DID OUR MISSILES GO?!"
      },
      {
        speaker: "Commander",
        text: "I need to review the security footage."
      },
      {
        text: "The security footage shows the missiles turning into a missile-shaped volume of water that immediately collapsed under the influence of gravity.",
        italicText: true
      },
      {
        speaker: "Commander",
        text: "WHAT?! HOW?! What technology are the Russians hiding?"
      }
    ],
    fullExitAnimationClass: "full-whiteFadeOut-5s",
    exitAnimationClass: "fadeOut-2_5s",
    nextNode: "choosePath-truth/ideals-0"
  },
  {
    name: "asteroid-peace-seeds-0",
    message: [
      {
        text: "Siberian Battlefield",
        italicText: true
      },
      {
        speaker: "Soldier",
        text: "Prepare to die!"
      },
      {
        text: "Soldier aims weapon at opposing commander, and pulls the trigger.",
        italicText: true
      },
      {
        text: "The 20 million soldiers' weapons simultaneously point towards the ground and fire tree seeds into the ground.",
        italicText: true
      },
      {
        text: "The planted tree seeds grow extremely quickly and create a dense forest on the battlefield.",
        italicText: true
      }
    ],
    fullExitAnimationClass: "full-whiteFadeOut-5s",
    exitAnimationClass: "fadeOut-2_5s",
    exitSound: sounds.whoosh,
    nextNode: "choosePath-truth/ideals-0"
  },
  {
    name: "asteroid-peace-jokes-0",
    messages: [
      {
        text: "United States Aircraft Carrier",
        italicText: true
      },
      {
        speaker: "Captain",
        text: "Fire the railgun!"
      },
      {
        text: "Nothing is fired. A voice booms from the railgun.",
        italicText: true
      },
      {
        speaker: "Railgun",
        text: "Knock knock."
      },
      {
        speaker: "Captain",
        text: "Umm... who's there?"
      },
      {
        speaker: "Railgun",
        text: "Who?"
      },
      {
        speaker: "Captain",
        text: "Who who?"
      },
      {
        speaker: "Railgun",
        text: "HA! You're an owl!"
      }
    ],
    fullExitAnimationClass: "full-whiteFadeOut-5s",
    exitAnimationClass: "fadeOut-2_5s",
    exitSound: sounds.whoosh,
    nextNode: "choosePath-truth/ideals-0"
  },
  {
    name: "asteroid-peace-print-0",
    messages: [
      {
        text: "Russian Nuclear Submarine",
        italicText: true
      },
      {
        speaker: "Captain",
        text: "Prepare for the submarine to surface! There's an oil spill, but we should be fine."
      },
      {
        speaker: "Captain",
        text: "Fire the nuclear missile!"
      },
      {
        speaker: "Soldier",
        text: "Firing in 3..2..1..."
      },
      {
        speaker: "Captain",
        text: "Where's the missile?"
      },
      {
        speaker: "Soldier",
        text: "I don't know, but there are a bunch of plastic pellets floating around us now. Looks like recycled plastic pellets used for 3D printing."
      },
      {
        speaker: "Captain",
        text: "What kind of special oil are the Americans using?"
      }
    ],
    fullExitAnimationClass: "full-whiteFadeOut-5s",
    exitAnimationClass: "fadeOut-2_5s",
    exitSound: sounds.whoosh,
    nextNode: "choosePath-truth/ideals-0"
  },
  {
    name: "choosePath-truth/ideals-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "So you are the type of person who wants to improve the world."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Do you wish to seek the truth and add to your civilization's knowledge of the universe?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Or do you want to create an ideal society?"
      }
    ],
    choices: [
      {
        text: "Truth",
        linkedNode: "truth-0",
        width: "8ch"
      },
      {
        text: "Ideals",
        linkedNode: "ideals-0",
        width: "8ch"
      }
    ]
  },
  {
    name: "choosePath-ideals/power-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "So you are the type of person who wants to control the world."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Do you wish to mold society to match your ideals?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Or do you seek power over the world?"
      }
    ],
    choices: [
      {
        text: "Ideals",
        linkedNode: "ideals-0",
        width: "8ch"
      },
      {
        text: "Power",
        linkedNode: "power-0",
        width: "8ch"
      }
    ]
  },
  {
    name: "truth-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "So you wish to seek out the universe's truth? That's a wise choice."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Do you want complete knowledge of mathematics?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Or would you like to know what is in humanity's immediate future?"
      }
    ],
    choices: [
      {
        text: "Mathematics",
        linkedNode: "truth-math-0",
        width: "18ch"
      },
      {
        text: "Humanity's Future",
        linkedNode: "truth-future-0",
        width: "18ch"
      }
    ]
  },
  {
    name: "ideals-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "So you wish to create a more ideal society? That's a nice choice."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Do you want everyone to have basic necessities such as food, water, shelter, and health care?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Or would you like all countries to be peacefully united?"
      }
    ],
    choices: [
      {
        text: "Necessities",
        linkedNode: "ideals-necessities-0",
        width: "18ch"
      },
      {
        text: "United Countries",
        linkedNode: "ideals-union-0",
        width: "18ch"
      }
    ]
  },
  {
    name: "power-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "So you wish to gain power? That's a selfish choice."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Do you want the power to persuade any humans you touch to do anything?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Or would you like to learn the most advanced techniques of war?"
      }
    ],
    choices: [
      {
        text: "Persuasion",
        linkedNode: "power-persuasion-0",
        width: "18ch"
      },
      {
        text: "War Knowledge",
        linkedNode: "power-warKnowledge-0",
        width: "18ch"
      }
    ]
  },
  {
    name: "truth-math-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "Calculus, linear algebra, topology, and more..."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "All of this universe's mathematical knowledge is now yours."
      }
    ],
    nextNode: "truth-math-1"
  },
  {
    name: "truth-math-1",
    messages: [
      {
        text: "A bright light surrounds you.",
        italicText: true
      }
    ],
    fullExitAnimationClass: "full-whiteFadeOut-5s",
    exitAnimationClass: "fadeOut-2_5s",
    backgroundSound: sounds.poweringUp,
    exitSound: sounds.whoosh,
    nextNode: "truth-math-2"
  },
  {
    name: "truth-math-2",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "You have now become enlightened with all the math knowledge there is in this universe."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Nice! I can now solve the Riemann hypothesis and so many other unsolved problems!"
      }
    ],
    nextNode: "truth-math-2"
  },
  {
    name: "truth-math-2",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "What would you like to do next? Develop a theory of everything, or create an industrial teleportation system?"
      }
    ],
    choices: [
      {
        text: "Theory of Everything",
        linkedNode: "truth-toe-0",
        width: "22ch"
      },
      {
        text: "Teleportation",
        linkedNode: "truth-teleport-0",
        width: "22ch"
      }
    ]
  },
  {
    name: "truth-future-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "So you wish to know the immediate future of humanity?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "That's an easy task for me."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Humans will master genetic manipulation technologies. Then, they will begin a period of artificial evolution that far outspeeds natural evolution."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Population growth will increase due to longer lifespans, leading to resource shortages."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "However, it is very likely that humans will destroy themselves before they become advanced enough to colonize space."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Looks like humanity's future will be kind of uncertain."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "I hope humanity's intelligence evolves fast enough for people to realize that the end is nearer than they thought."
      }
    ],
    nextNode: "truth-future-1"
  },
  {
    name: "truth-future-1",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "Now, what would you like to do next?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "You can construct an industrial teleportation machine, or end pollution and climate change."
      }
    ],
    choices: [
      {
        text: "Teleportation",
        linkedNode: "truth-teleport-0",
        width: "22ch"
      },
      {
        text: "Climate",
        linkedNode: "ideals-climate-0",
        width: "22ch"
      }
    ]
  },
  {
    name: "truth-toe-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "You seek the theory of everything. A theory of quantum gravity."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Well, you know what to do now. Let the light enlighten you once more."
      }
    ],
    nextNode: "truth-toe-1"
  },
  {
    name: "truth-toe-1",
    messages: [
      {
        text: "A glowing light surrounds you.",
        italicText: true
      }
    ],
    fullExitAnimationClass: "full-whiteFadeOut-5s",
    exitAnimationClass: "fadeOut-2_5s",
    backgroundSound: sounds.poweringUp,
    exitSound: sounds.whoosh,
    nextNode: "truth-toe-2"
  },
  {
    name: "truth-toe-2",
    messages: [
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Wow, I have so much knowledge now."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "I know exactly how this universe works."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "But I also know that I cannot know the exact future."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Yes, quantum physics is truly undeterministic. Predicting quantum systems is the one barrier no one will ever be able to cross."
      }
    ],
    nextNode: "truth-toe-2"
  },
  {
    name: "truth-toe-2",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "What is the next thing you seek?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Knowledge of everything in existence, including the multiverse?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Knowledge of what human civilization will be in hundreds of thousands of years?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Or the ability to travel to parallel universes?"
      }
    ],
    choices: [
      {
        text: "Multiverse",
        linkedNode: "truth-koe-0",
        width: "22ch"
      },
      {
        text: "Human Civilization",
        linkedNode: "truth-blackHole-0",
        width: "22ch"
      },
      {
        text: "Parallel Universes",
        linkedNode: "ideals-parallelUniverses-0",
        width: "22ch"
      }
    ]
  },
  {
    name: "truth-teleport-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "Building a teleportation device, now that's an exciting task."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Here are the blueprints for the machine. Also, an instruction manual."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "I will also immediately make you one. Patent the blueprints or share it with other people."
      },
      {
        text: "A huge ring-shaped device appears.",
        italicText: true
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Amazing, this device will revolutionize transportation and boost the world's economies!"
      }
    ],
    nextNode: "truth-teleport-1"
  },
  {
    name: "truth-teleport-1",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "What is the next thing you seek?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Knowledge of what human civilization will be in hundreds of thousands of years?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "An eternal energy source?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Or the ability to travel to parallel universes?"
      }
    ],
    choices: [
      {
        text: "Human Civilization",
        linkedNode: "truth-blackHole-0",
        width: "22ch"
      },
      {
        text: "Eternal Energy",
        linkedNode: "ideals-eternalEnergy-0",
        width: "22ch"
      },
      {
        text: "Parallel Universes",
        linkedNode: "ideals-parallelUniverses-0",
        width: "22ch"
      }
    ]
  },
  {
    name: "truth-koe-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "This will be the last bit of knowledge I can give you. Knowledge of not just this universe, but the multiverse."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Are you ready to learn the truth about the multiverse?"
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Yes."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Let's begin. So, in the beginning, there was -"
      }
    ],
    noInteractionExit: true,
    nextNode: "truth-ending-simulation-0"
  },
  {
    name: "truth-blackHole-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "Let's make a spaceship so we can travel into the future."
      },
      {
        text: "A spaceship appears.",
        italicText: true
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Well, what are you waiting for? Get in."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "How is a spaceship supposed to let us travel into the future?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "You see, around a rotating black hole is a region called the ergosphere. The frame-dragging effect is increased in this region, which also permits us to travel far into the future in a short time."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Oh, I was confused for a moment."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Well, let's go!"
      },
      {
        text: "The spaceship launches from Earth, accelerated by {{BLOB_NAME}}'s powers, heading towards a supermassive black hole.",
        italicText: true
      },
      {
        text: "The spaceship spends a few days near the black hole before returning.",
        italicText: true
      },
      {
        text: "The entire trip only takes a few weeks for the occupants thanks to relativistic time dilation.",
        italicText: true
      }
    ],
    nextNode: "truth-ending-endOfHumanity-0"
  },
  {
    name: "truth-ending-simulation-0",
    messages: [
      {
        speaker: "System",
        text: "Application \"universe-simulation\" (PID 29385, child process of universe) attempted to access unauthorized network resources. Permission denied by SMB server."
      },
      {
        speaker: "System",
        text: "Terminating service universe (PID 29384)..."
      },
      {
        speaker: "System",
        text: "----- END LOG -----"
      },
      {
        speaker: "System",
        text: "root@universe:/# "
      }
    ],
    nextNode: "truth-ending-simulation-1"
  },
  {
    name: "truth-ending-simulation-1",
    messages: [
      {
        text: "And so, the simulation of the universe ended.",
        italicText: true
      },
      {
        text: "At least, until Jack comes back from his lunch break and restarts the simulation after fixing the error.",
        italicText: true
      }
    ],
    exitAnimationClass: "fadeOut-2_5s",
    nextNode: "end-0"
  },
  {
    name: "truth-ending-endOfHumanity-0",
    messages: [
      {
        text: "Spaceship orbiting a planet, 424424 CE",
        italicText: true
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "We have arrived over 420000 years in the future."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Are we orbiting Venus? Look at all those sulfuric acid clouds."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "No, this is Earth."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "This can't possibly be Earth!"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "But it is. Humans could not stop polluting the Earth."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Although scientists had claimed that carbon dioxide emissions could not cause a runaway greenhouse effect, humans began creating large quantities of a much more insulating gas to place in their building's walls."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Over time, this gas leaked out of the walls, and by the time people noticed, it was too late."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Humans had sealed their own fates."
      }
    ],
    nextNode: "truth-ending-endOfHumanity-1"
  },
  {
    name: "truth-ending-endOfHumanity-0",
    messages: [
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Is there anything that we can do to prevent this tragedy?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "If you are thinking about going back in time, forget about it."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Backwards time travel is prohibited by the fundamental laws of physics."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "So what do we do now? There is nowhere to return to. Can you restore Earth and its former living inhabitants?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "I can restore Earth, but I cannot restore life. That is the limit of my power."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "(Well, there is absolutely nothing left in this universe for me...)"
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "(All of my friends and family... Doug... Even Karen... They're all gone.)"
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "(There is no life for me to return to.)"
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "There is nothing left here... Take me to the end of the universe."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "As you wish. Our final destination is on the other side of a black hole's event horizon."
      }
    ],
    exitAnimationClass: "fadeOut-2_5s",
    nextNode: "end-0"
  },
  {
    name: "ideals-necessities-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "For food, everyone on Earth has received devices that generate food from solar energy."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "For water, I have created many desalination plants that employ carbon nanotubes to efficiently filter salt water."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "For the homeless, they have received almost-magical tents that bend spacetime to provide the equivalent shelter and comfort to a modern house."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Finally, for health care, everyone now has nanobots in their blood stream that supplement the immune system."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Wouldn't all of these be vulnerable to solar flares?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "No, they have been hardened against electromagnetic interference."
      }
    ],
    nextNode: "ideals-necessities-1"
  },
  {
    name: "ideals-necessities-1",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "Now, what would you like to do next?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "You can eliminate all pollution and stop climate change, end all diseases, or create an industrial teleportation device."
      }
    ],
    choices: [
      {
        text: "Climate",
        linkedNode: "ideals-climate-0",
        width: "22ch"
      },
      {
        text: "End Diseases",
        linkedNode: "ideals-endDisease-0",
        width: "22ch"
      },
      {
        text: "Teleportation",
        linkedNode: "truth-teleport-0",
        width: "22ch"
      }
    ]
  },
  {
    name: "ideals-union-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "All countries will now be united."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "War and conflicts will be no more."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Everyone will be required to follow our beliefs so that there can be no sides for people to choose and fight for."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "This entire process will take a while though, taking until 4891 to fully complete."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "In the meantime though, global peace will be predominant."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Why must it require so much time?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Humans take a long time to change, but eventually, they will change."
      }
    ],
    nextNode: "ideals-union-1"
  },
  {
    name: "ideals-union-1",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "Now, what would you like to do next?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "You can become a dictator, or end pollution and climate change."
      }
    ],
    choices: [
      {
        text: "Dictator",
        linkedNode: "power-dictator-0",
        width: "22ch"
      },
      {
        text: "Climate",
        linkedNode: "ideals-climate-0",
        width: "22ch"
      }
    ]
  },
  {
    name: "ideals-climate-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "First, I will convert all the pollution in oceans and rivers to pure water."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Now, I will turn a large portion of the greenhouse gases in the Earth's atmosphere into pure oxygen."
      },
      {
        text: "A fresh breeze causes the trees to wave.",
        italicText: true
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "I can feel the fresh air against my face!"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Yes, this is what air is supposed to feel like."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Your ancient ancestors from thousands of years ago breathed in this pure air."
      }
    ],
    nextNode: "ideals-climate-1"
  },
  {
    name: "ideals-climate-1",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "What is the next thing you seek?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "A source of eternal and infinite energy?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Spreading the human population across parallel universes?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Or enslaving the world to maintain nature?"
      }
    ],
    choices: [
      {
        text: "Eternal Energy",
        linkedNode: "ideals-eternalEnergy-0",
        width: "22ch"
      },
      {
        text: "Parallel Universes",
        linkedNode: "ideals-parallelUniverses-0",
        width: "22ch"
      },
      {
        text: "Enslave World",
        linkedNode: "power-enslaveWorld-0",
        width: "22ch"
      }
    ]
  },
  {
    name: "ideals-endDisease-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "Ending all of the world's diseases... That's a noble choice."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "I will destroy all harmful viruses, prions, and bacteria."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "And now, I eliminate all the things human immune systems cannot remove."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Cancers, genetic diseases, malnutrition... these are all gone too."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "I don't feel any different."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "You will feel more energetic after a few days."
      }
    ],
    nextNode: "ideals-endDisease-1"
  },
  {
    name: "ideals-endDisease-1",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "What is the next thing you seek?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "A source of eternal and infinite energy?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Spreading the human population across parallel universes?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Or knowing everything about existence and the multiverse?"
      }
    ],
    choices: [
      {
        text: "Eternal Energy",
        linkedNode: "ideals-eternalEnergy-0",
        width: "22ch"
      },
      {
        text: "Parallel Universes",
        linkedNode: "ideals-parallelUniverses-0",
        width: "22ch"
      },
      {
        text: "Knowledge",
        linkedNode: "truth-koe-0",
        width: "22ch"
      }
    ]
  },
  {
    name: "ideals-eternalEnergy-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "So you want an eternal source of energy?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Then I'll make you an orb that can provide infinite energy!"
      },
      {
        text: "{{BLOB_NAME}} focuses and an orb of energy begins to form.",
        italicText: true
      },
      {
        text: "The orb glows brighter and brighter.",
        italicText: true
      }
    ],
    nextNode: "ideals-ending-groundState-0"
  },
  {
    name: "ideals-parallelUniverses-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "Parallel universes... the final frontier of your species."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Let's make the first contact with a parallel universe."
      },
      {
        text: "A spherical distortion appears in front of you.",
        italicText: true
      },
      {
        text: "{{BLOB_NAME}} is concentrating on the distortion, feeding more energy into it.",
        italicText: true
      }
    ],
    nextNode: "ideals-ending-parallelInvasion-0"
  },
  {
    name: "ideals-ending-groundState-0",
    messages: [
      {
        text: "The immense energy density causes the metastable vacuum ground state to quantum tunnel to a lower energy value.",
        italicText: true
      },
      {
        text: "The bubble of decaying ground state expands outwards at the speed of light, destroying everything.",
        italicText: true
      },
      {
        text: "Inside the bubble, a new universe with wildly different properties is born.",
        italicText: true
      },
      {
        text: "A familiar, yet different, consciousness fades away in that new universe.",
        italicText: true
      }
    ],
    messageGroupOuterContainerClass: "heavyQuake-1s",
    exitAnimationClass: "fadeOut-2_5s",
    backgroundSound: sounds.quake,
    nextNode: "end-0"
  },
  {
    name: "ideals-ending-parallelInvasion-0",
    messages: [
      {
        text: "A wormhole appears in front of you.",
        italicText: true
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Let's go through and explore the new world!"
      },
      {
        text: "You feel a flash of pain in your chest and fall to the ground.",
        italicText: true
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "{{PLAYER_NAME}}, you've been shot by an energy weapon!"
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "(What... is... happening?)"
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "{{BLOB_NAME}}, help me..."
      },
      {
        text: "A black hole is shot out of the wormhole and absorbs {{BLOB_NAME}}, flying off into space.",
        italicText: true
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "(NO!!! It's all over. For all of us.)"
      },
      {
        text: "An army from the parallel universe marches through the wormhole, with advanced weapons.",
        italicText: true
      },
      {
        text: "As your consciousness fades away, you see the invaders begin to take over your world.",
        italicText: true
      }
    ],
    exitAnimationClass: "fadeOut-2_5s",
    nextNode: "end-0"
  },
  {
    name: "power-persuasion-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "So you want the power of persuasion? That should be easy."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "I will now grant you the power to secrete a chemical that allows you to persuade anyone to do anything."
      }
    ],
    nextNode: "power-persuasion-1"
  },
  {
    name: "power-persuasion-1",
    messages: [
      {
        text: "A glowing light begins to surround you.",
        italicText: true
      }
    ],
    fullExitAnimationClass: "full-whiteFadeOut-5s",
    exitAnimationClass: "fadeOut-2_5s",
    backgroundSound: sounds.poweringUp,
    exitSound: sounds.whoosh,
    nextNode: "power-persuasion-1"
  },
  {
    name: "power-persuasion-1",
    messages: [
      {
        text: "The light disappears.",
        italicText: true
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "So I have the power now?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Yes."
      }
    ],
    nextNode: "power-persuasion-2"
  },
  {
    name: "power-persuasion-2",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "Now, what would you like to do next?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "You can end pollution and climate change and then use your new powers to persuade people to maintain the environment, or use your powers to take over the world and become a dictator."
      }
    ],
    choices: [
      {
        text: "Climate",
        linkedNode: "ideals-climate-0",
        width: "22ch"
      },
      {
        text: "Dictator",
        linkedNode: "power-dictator-0",
        width: "22ch"
      },
      {
        text: "Teleportation",
        linkedNode: "truth-teleport-0",
        width: "22ch"
      }
    ]
  },
  {
    name: "power-warKnowledge-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "I will give you the most advanced war techniques that exist."
      }
    ],
    nextNode: "power-warKnowledge-1"
  },
  {
    name: "power-warKnowledge-1",
    messages: [
      {
        text: "A glowing light surrounds you.",
        italicText: true
      }
    ],
    fullExitAnimationClass: "full-whiteFadeOut-5s",
    exitAnimationClass: "fadeOut-2_5s",
    backgroundSound: sounds.poweringUp,
    exitSound: sounds.whoosh,
    nextNode: "power-warKnowledge-2"
  },
  {
    name: "power-warKnowledge-2",
    messages: [
      {
        text: "The light fades away.",
        italicText: true
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Wow, I know so many things about war now. I feel like I can command an army to conquer the world."
      }
    ],
    nextNode: "power-warKnowledge-2"
  },
  {
    name: "power-warKnowledge-2",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "Now, what would you like to do next?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "You can become a dictator of the entire world, or create war robots."
      }
    ],
    choices: [
      {
        text: "Dictator",
        linkedNode: "power-dictator-0",
        width: "22ch"
      },
      {
        text: "War Robots",
        linkedNode: "power-warRobots-0",
        width: "22ch"
      },
      {
        text: "Teleportation",
        linkedNode: "truth-teleport-0",
        width: "22ch"
      }
    ]
  },
  {
    name: "power-dictator-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "You're really going to become a dictator?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "I suppose I can create a robotic police force and an advanced surveillance system so that you can maintain your power."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Wow, this is amazing! I can change the world to suit my ideals."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "I would be careful if I were you."
      }
    ],
    nextNode: "power-dictator-1"
  },
  {
    name: "power-dictator-1",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "What is the next thing you seek?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Enslaving the world?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Creating the best weapon to exist?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Or colonizing parallel universes?"
      }
    ],
    choices: [
      {
        text: "Enslave World",
        linkedNode: "power-enslaveWorld-0",
        width: "22ch"
      },
      {
        text: "Weapon",
        linkedNode: "power-ultimateWeapon-0",
        width: "22ch"
      },
      {
        text: "Parallel Universes",
        linkedNode: "ideals-parallelUniverses-0",
        width: "22ch"
      }
    ]
  },
  {
    name: "power-warRobots-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "Creating war robots... this will be fun."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Here is a tiny drone that uses facial recognition to terminate targets."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "And this is an autonomous tank that can fire antimatter projectiles."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Finally, these are robotic spies. They are indistinguishable from humans."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Amazing technology! I can rule the world with this."
      }
    ],
    nextNode: "power-warRobots-1"
  },
  {
    name: "power-warRobots-1",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "What is the next thing you seek?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Enslaving the world?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Creating the best weapon to exist?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Or creating a source of eternal and infinite energy?"
      }
    ],
    choices: [
      {
        text: "Enslave World",
        linkedNode: "power-enslaveWorld-0",
        width: "22ch"
      },
      {
        text: "Weapon",
        linkedNode: "power-ultimateWeapon-0",
        width: "22ch"
      },
      {
        text: "Eternal Energy",
        linkedNode: "ideals-eternalEnergy-0",
        width: "22ch"
      }
    ]
  },
  {
    name: "power-enslaveWorld-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "You wish to enslave the entire world?"
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Of course! What are you waiting for?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "As you wish.",
        italicText: true
      }
    ],
    nextNode: "power-ending-slavery-0"
  },
  {
    name: "power-ultimateWeapon-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "I will now create the best weapon in existence!"
      },
      {
        text: "A massive weapon grows out of the ground, pointing towards the sky.",
        italicText: true
      },
      {
        text: "The weapon draws in energy from its surroundings.",
        italicText: true
      },
      {
        text: "{{BLOB_NAME}} feeds the weapon more energy.",
        italicText: true
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Ready to fire the latest ultimate weapon?"
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Yes."
      }
    ],
    nextNode: "power-ending-strangelets-0"
  },
  {
    name: "power-ending-slavery-0",
    messages: [
      {
        speaker: "{{BLOB_NAME}}",
        text: "Wow, you've really done it. The world is under your complete control now."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "How about I join in the fun too?"
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "(Wait. What?)"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "You're my slave now. All of you are."
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Now dance for me."
      }
    ],
    exitAnimationClass: "fadeOut-2_5s",
    nextNode: "end-0"
  },
  {
    name: "power-ending-strangelets-0",
    messages: [
      {
        text: "A beam of primordial energy blasts into the sky.",
        italicText: true
      },
      {
        text: "Some blobs of matter form under the intense conditions and rain back down to Earth.",
        italicText: true
      },
      {
        text: "Upon contact with the ground, the blobs spread across the surface of the Earth.",
        italicText: true
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Those blobs... they're the strangelets we have hypothesized about, aren't they?"
      },
      {
        speaker: "{{BLOB_NAME}}",
        text: "Yes, they are. They are the most stable form of matter, and are catalyzing the conversion of all matter into more strangelets."
      },
      {
        speaker: "{{PLAYER_NAME}}",
        text: "Well goodbye; it's the end of the world."
      },
      {
        text: "The stangelets consume the entire Earth, turning it into an extremely dense ball of matter, 1 meter in radius.",
        italicText: true
      }
    ],
    messageGroupOuterContainerClass: "heavyQuake-1s",
    exitAnimationClass: "fadeOut-2_5s",
    backgroundSound: sounds.quake,
    nextNode: "end-0"
  },
  {
    name: "end-0",
    messages: [
      {
        speaker: "THE END",
        boldText: true,
        fontSize: "128px"
      }
    ]
  }
];

function formatNodeMessages(node) {
  if (!node._originalMessages) {
    node._originalMessages = node.messages.map(o => ({...o}));
  } else {
    node.messages = node._originalMessages.map(o => ({...o}));
  }
  let i = node.messages.length;
  while (i--) {
    const speaker = node.messages[i].speaker?.replace(/\{\{PLAYER_NAME\}\}/g, store.state.playerName).replace(/\{\{BLOB_NAME\}\}/g, store.state.blobName);
    let speakerLength = 0;
    if (typeof speaker === "string") {
      speakerLength = speaker.length + 2;
    }
    const text = node.messages[i].text?.replace(/\{\{PLAYER_NAME\}\}/g, store.state.playerName).replace(/\{\{BLOB_NAME\}\}/g, store.state.blobName);
    if (text && speakerLength + text.length > 115) {
      const {italicText, boldText, continuation} = node.messages[i];
      let j = 0;
      let words = node.messages[i].text.split(" ");
      let lines = [words.shift()];
      while (words.length > 0) {
        const word = words.shift();
        if (lines[j].length + word.replace(/\{\{PLAYER_NAME\}\}/g, store.state.playerName).replace(/\{\{BLOB_NAME\}\}/g, store.state.blobName).length + (j === 0 ? speakerLength : 0) < 115) {
          lines[j] += ` ${word}`;
        } else {
          j++;
          lines[j] = word;
        }
      }
      node.messages.splice(i, 1);
      let lineIndex = lines.length;
      while (lineIndex--) {
        node.messages.splice(i, 0, {
          speaker: lineIndex === 0 ? speaker : undefined,
          text: lines[lineIndex],
          italicText,
          boldText,
          continuation: lineIndex === 0 ? !!continuation : true // Makes multiple split lines act as one line.
        });
      }
    }
  }
}

export function getNode(name) {
  const node = nodes.find(n => n.name === name);
  if (!node) {
    throw new Error(`No node with name ${name}.`);
  }
  return node;
}

export function goToNode(name) {
  const node = getNode(name);
  formatNodeMessages(node);
  store.commit("setCurrentNode", {
    nodeName: name,
    save: node.save ?? true
  });
}
