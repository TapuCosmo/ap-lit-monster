"use strict";

import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    playerName: window.localStorage.playerName || "Player",
    blobName: window.localStorage.blobName || "Blob",
    currentNode: "splash-0"
  },
  mutations: {
    setPlayerName(state, name) {
      state.playerName = name;
      window.localStorage.playerName = name;
    },
    setBlobName(state, name) {
      state.blobName = name;
      window.localStorage.blobName = name;
    },
    setCurrentNode(state, {nodeName, save}) {
      state.currentNode = nodeName;
      if (save) {
        window.localStorage.currentNode = nodeName;
      }
    }
  }
});
