"use strict";

import Vue from "vue";
import App from "./App.vue";
import sounds from "@/sounds.js";
import store from "./store";
import {goToNode} from "@/nodes.js";

window.store = store;
window.debug = {
  goToNode,
  sounds
};

Vue.config.productionTip = false;

Vue.directive("visibility", function (el, binding) {
  el.style.visibility = binding.value ? "visible" : "hidden";
});

new Vue({
  store,
  render: h => h(App)
}).$mount("#app");
