module.exports = {
  publicPath: process.env.NODE_ENV === "production" ?
    "/ap-lit-monster/" :
    "/"
};
